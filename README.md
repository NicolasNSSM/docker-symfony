HOWTO
=====

In web/app_dev.php, check if env variable DOCKER_ENV ```getenv('DOCKER_ENV')``` exists, if so, make it escape the 403 Forbidden trap.
 
In app/config/parameters.yml.docker :
```yaml
parameters:
    database_host: mariadb
    database_port: null
    database_user: root
    database_password: password
```


